import neat
import pickle
import sys
import retro
import numpy as np
import cv2


def run_winner(n=1):
    # Load configuration.
    config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                         neat.DefaultSpeciesSet, neat.DefaultStagnation,
                         'config-feedforward')

    genome = pickle.load(open('top_agent.pkl', 'rb'))

    env = retro.make('SuperMarioBros-Nes', state='Level1-1')
    obs = env.reset()
    inx, iny, inc = env.observation_space.shape

    inx = int(inx / 8)
    iny = int(iny / 8)

    net = neat.nn.recurrent.RecurrentNetwork.create(genome, config)
    frame = 0
    done = False

    while not done:

        env.render()
        frame += 1

        scaledimg = cv2.cvtColor(obs, cv2.COLOR_BGRA2RGBA)
        scaledimg = cv2.resize(scaledimg, (iny, inx))

        obs = cv2.resize(scaledimg, (inx, iny))
        obs = cv2.cvtColor(obs, cv2.COLOR_BGR2GRAY)
        obs = np.reshape(obs, (inx, iny))

        imgarray = np.ndarray.flatten(obs)

        nnOutput = net.activate(imgarray)
        nnOutput = [0.0 for i in range(6)] + nnOutput

        obs, rew, done, info = env.step(nnOutput)


def main():
    if len(sys.argv)>1:
        run_winner(int(sys.argv[1]))
    else:
        run_winner()

if __name__ == "__main__":
    main()
