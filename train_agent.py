import pickle
import retro
import neat
import numpy as np
import cv2
import visualize


env = retro.make('SuperMarioBros-Nes', state='Level1-1')


def eval_genomes(genomes, config):
    for genome_id, genome in genomes:

        obs = env.reset()
        inx, iny, inc = env.observation_space.shape

        inx = int(inx / 8)
        iny = int(iny / 8)

        net = neat.nn.recurrent.RecurrentNetwork.create(genome, config)

        fitness_current = 0
        current_max_fitness = 0

        frame = 0
        counter = 0
        done = False

        while not done:
            env.render()
            frame += 1

            scaledimg = cv2.cvtColor(obs, cv2.COLOR_BGRA2RGBA)
            scaledimg = cv2.resize(scaledimg, (iny, inx))

            obs = cv2.resize(scaledimg, (inx, iny))
            obs = cv2.cvtColor(obs, cv2.COLOR_BGR2GRAY)
            obs = np.reshape(obs, (inx, iny))

            imgarray = np.ndarray.flatten(obs)

            nnOutput = net.activate(imgarray)

            # 3 outputs implies that there will be 3 buttons with 2 different states: pressed and not pressed
            # [0, 0, 0] -> Left, Right, B (for jumping)

            # possible combinations assuming 9 button NES layout, with 1 representing a pressed button:
            # stand still, move left, move right, jump in place, jump left, jump right:
            # [0,0,0,0,0,0,0,0,0],
            # [0,0,0,0,0,0,1,0,0],
            # [0,0,0,0,0,0,0,1,0],
            # [0,0,0,0,0,0,0,0,1],
            # [0,0,0,0,0,0,1,0,1],
            # [0,0,0,0,0,0,0,1,1],

            # nnOutput will be converted from a 3-length array to 9-length.
            # everything else is set to 0

            nnOutput = [0.0 for i in range(6)] + nnOutput
            obs, rew, done, info = env.step(nnOutput)

            fitness_current += rew

            if fitness_current > current_max_fitness:
                current_max_fitness = fitness_current
                counter = 0
            else:
                counter += 1

            if done or counter == 150 or fitness_current > 135:
                done = True

            genome.fitness = fitness_current


config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                     neat.DefaultSpeciesSet, neat.DefaultStagnation,
                     'config-feedforward')

# p = neat.Checkpointer.restore_checkpoint('neat-checkpoint-86')
p = neat.Population(config)

p.add_reporter(neat.StdOutReporter(True))
stats = neat.StatisticsReporter()
p.add_reporter(stats)
p.add_reporter(neat.Checkpointer(10))

winner = p.run(eval_genomes, 1)

#node_names = {}
#visualize.draw_net(config, winner, True, node_names=node_names)
#visualize.plot_stats(stats, ylog=False, view=True)
#visualize.plot_species(stats, view=True)

with open('top_agent.pkl', 'wb') as output:
    pickle.dump(winner, output, 1)
